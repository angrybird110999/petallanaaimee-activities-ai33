<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categories;
class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [

            'English','Filipino','Math'
        ];
        foreach($categories as $category) {
            Categories::create(['category' => $category]);
        }
    }
}

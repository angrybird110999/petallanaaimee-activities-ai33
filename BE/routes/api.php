<?php

use App\Http\Controllers\BooksController;
use App\Http\Controllers\BorrowBookController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\ReturnedBookController;
use Illuminate\Support\Facades\Route;

Route::resource('Books', BooksController::class);
Route::resource('Patrons', PatronController::class);
Route::resource('Categories', CategoriesController::class);
Route::resource('Borrowed_Book', BorrowBookController::class);
Route::resource('Returned_Books', ReturnedBookController::class);



<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patrons extends Model
{
    use HasFactory;
    protected  $fillable = ['last_name', 'first_name', 'middle_name', 'email'];


    public function returned()
    {
        return $this->hasMany(Returned_Books::class, 'patron_id');
    }
    
    public function borrowed()
    {
        return $this->hasMany(Borrowed_Books::class, 'patron_id');
    }
}

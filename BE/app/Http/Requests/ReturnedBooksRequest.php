<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnedBooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patron_id' => ['required', 'exists:Patrons,id', 'integer'],
            'copies' => ['required', 'min:1', 'integer'],
            'book_id' => ['required', 'exists:Books,id', 'integer'],
        ];
    }

    public  function messages()
    {
        return [
            'patron_id.required' => 'Patron ID is required',
            'patron_id.exists' => 'Patron ID does not exist exist', 
            'copies.required' => 'Number of Copies is required',
            'book_id.required' => 'Book ID is required',
            'book_id.exists' => 'Book ID does not exist exist', 
        ];
    }
}

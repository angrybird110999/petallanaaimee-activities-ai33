<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          
            'name' => ['required', Rule::unique('Books')->ignore($this->id),' min:2', 'max:255'],
            'author' => ['required',' min:2', 'max:255'],
            'copies' => ['required' ,'min:1', 'integer'],
            'category_id' => ['required', 'exists:Categories,id'],
        ];
    }

    public  function messages()
    {
        return [
            
            'name.required' => 'Fill up the Book Name',
            'name.unique' => 'Book Already Exist',
            'author.required' => 'Fill up the Book Author',
            'copies.required' => 'Fill up the Book Copies',
            'copies.integer' => 'Fill up the copies with number',
            'category_id.required' => 'The Book must belong to category', 
            'category_id.exists' => 'Caterogy did not exist', 
        ];
    }
}

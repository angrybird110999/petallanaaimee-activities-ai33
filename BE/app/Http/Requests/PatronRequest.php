<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class PatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => ['required',' min:2', 'max:255'],
            'first_name' => ['required',' min:2', 'max:255'],
            'middle_name' => ['required',' min:2', 'max:255'],
            'email' => ['required', 'email', 'min:8', 'max:255' , Rule::unique('Patrons')->ignore($this->id)]
        ];
    }

    public  function messages()
    {
        return [
            
            'last_name.required' => 'Last Name of Patron is required',
            'first_name.required' => 'First Name of Patron is required',
            'middle_name.required' => 'Middle Name of Patron is required',
            'email.required' => 'Email of Patron is required',
            'email.email' => 'Please input valid email',

        ];
    }
}

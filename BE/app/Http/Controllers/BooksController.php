<?php

namespace App\Http\Controllers;

use App\Http\Requests\BooksRequest;
use App\Models\Books;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Books = Books::with('category')->orderBy('created_at', 'desc')->get();
        return response()->json($Books);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     *@param  \App\Http\Requests\BooksRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BooksRequest $request)
    {
        $Book = Books::create($request->validated());
        return response()->json(['message' => 'Book has been saved', 'data' => Books::with('category')->get()->where('id', $Book->id)->first()]);
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Books $Book)
    {
        $Book = Books::with('category')->get()->where('id', $Book->id);
        return response()->json($Book);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\BooksRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BooksRequest $request, Books $Book)
    {

        $Book->update($request->validated());
        return response()->json(['message' => 'Book has been Updated', 'data' => Books::with('category')->get()->where('id', $Book->id)->first()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Books $Book)
    {
        $Book->delete();
        return response()->json(['message' => 'Books  has been Deleted', 'data' => $Book]);
    }
}

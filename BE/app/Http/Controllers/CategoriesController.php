<?php

namespace App\Http\Controllers;
use App\Http\Requests\CategoriesRequest;
use App\Models\Categories;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $Categories = Categories::orderBy('category')->get();
        return response()->json($Categories);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CategoriesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request )
    {   

        $Categories = Categories::create($request->validated());
        return response()->json(['message' => 'Categories Saved', 'data' => $Categories]);
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $Category)
    {
        return response()->json($Category);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CategoriesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, Categories $Category)
    {
        $Category->update($request->validated());
        return response()->json(['message' => 'Category Updated', 'data' => $Category]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $Category)
    {
        $Category->delete();
        return response()->json(['message' => 'Category Delete', 'data' => $Category]);
    }
}

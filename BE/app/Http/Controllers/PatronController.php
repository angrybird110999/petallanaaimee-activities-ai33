<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatronRequest;
use App\Models\Patrons;
class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $Patrons = Patrons::with('borrowed','returned')->orderBy('created_at', 'desc')->get();
        return response()->json($Patrons);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PatronRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronRequest $request)
    {
        $Patron = Patrons::create($request->validated());
        return response()->json(['message' => 'Patron has been Saved', 'data' =>  $Patron]);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patrons $Patron)
    {
        $Patron = Patrons::with('borrowed','returned')->get()->where('id', $Patron->id)->sortByDesc("id");
        return response()->json($Patron);
        
    }

    /**
     * Update the specified resource in storage.
     *
     *  @param  \App\Http\Requests\PatronRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $request, Patrons $Patron)
    {
    
        $Patron->update($request->validated());
        return response()->json(['message' => 'Patron Updated', 'data' => $Patron]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patrons $Patron)
    {
        $Patron->delete();
        return response()->json(['message' => 'Patron Delete', 'data' => $Patron]);
    }
}

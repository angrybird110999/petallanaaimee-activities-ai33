import Api from "./Api";
const Base = "Books";
export default {
  index() {
    return Api.get(Base);
  },
  store(Book) {
    return Api.post(Base, Book);
  },
  update(Book) {
    return Api.put(`${Base}/${Book.id}`, Book);
  },
  destroy(Book) {
    return Api.delete(`${Base}/${Book.id}`);
  },
};

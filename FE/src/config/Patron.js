import Api from "./Api";
const Base = "Patrons";
export default {
  index() {
    return Api.get(Base);
  },
  store(Patron) {
    return Api.post(Base, Patron);
  },
  update(Patron) {
    return Api.put(`${Base}/${Patron.id}`, Patron);
  },
  destroy(Patron) {
    return Api.delete(`${Base}/${Patron.id}`);
  },
};

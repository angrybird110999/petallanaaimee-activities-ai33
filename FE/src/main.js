import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Vue from "vue";
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";
import Toast, { TYPE } from "vue-toastification";
import "vue-toastification/dist/index.css";
import App from "./../src/App.vue";
import router from "./routes/router";
import store from "./store";

const options = {
  transition: "Vue-Toastification__slideBlurred",
  maxToasts: 5,
  newestOnTop: true,
  position: "bottom-right",
  toastDefaults: {
    // ToastOptions object for each type of

    [TYPE.ERROR]: {
      timeout: 5000,
      closeOnClick: true,
      pauseOnFocusLoss: false,
      pauseOnHover: false,
      draggable: false,
      draggablePercent: 0.6,
      showCloseButtonOnHover: false,
      hideProgressBar: true,
      closeButton: "button",
      icon: true,
      rtl: false,
    },
    [TYPE.SUCCESS]: {
      timeout: 3000,
      closeOnClick: true,
      pauseOnFocusLoss: false,
      pauseOnHover: false,
      draggable: true,
      draggablePercent: 0.6,
      showCloseButtonOnHover: false,
      hideProgressBar: true,
      closeButton: "button",
      icon: true,
      rtl: false,
    },
  },
};

Vue.component("v-select", vSelect);

Vue.use(Toast, options);
Vue.use(require("vue-moment"));
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;
new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");

import { formFieldMixin } from "../../assets/js/validationscript.js";
import { mapGetters, mapActions } from "vuex";
export default {
    mixins: [formFieldMixin],
    data() {
      return {
        PatronForm: {
          last_name: "",
          first_name: "",
          middle_name: "",
          email: "",
        },
      };
    },
    methods: {
      ...mapActions([
        "fetchPatrons",
        "AddOnePatron",
        "UpdateOnePatron",
        "RemoveOnePatron",
      ]),
      AddPatron() {
        if (
          this.PatronForm.first_name == "" ||
          this.PatronForm.middle_name == "" ||
          this.PatronForm.last_name == "" ||
          this.PatronForm.PatronEmail == ""
        ) {
          this.ErrorToastr();
        } else {
          this.AddToast(this.PatronForm.first_name);
          this.AddOnePatron(this.PatronForm);
         
          this.HideModal();
        }
      },
      UpdatePatron() {
        if (
          this.PatronForm.first_name == "" ||
          this.PatronForm.middle_name == "" ||
          this.PatronForm.last_name == "" ||
          this.PatronForm.PatronEmail == ""
        ) {
          this.ErrorToastr();
        } else {
          this.UpdateOnePatron(this.PatronForm);
          this.UpdateToast(this.PatronForm.first_name);
          this.HideModal();
        }
      },
      DeletePatron(Patron) {
        this.DeleteToaster();
        this.RemoveOnePatron(Patron);
        this.HideModal();
      },
    },
    mounted() {
      this.fetchPatrons();
    },
    computed: {
      ...mapGetters(["LoadingStatus", "AllPatrons"]),
      PatronFormFilter: function() {
        var self = this;
        return this.AllPatrons.filter(function(Patron) {
          return (
            Patron.first_name.toLowerCase().indexOf(self.Search.toLowerCase()) >=
              0 ||
            Patron.last_name.toLowerCase().indexOf(self.Search.toLowerCase()) >=
              0 ||
            Patron.middle_name.toLowerCase().indexOf(self.Search.toLowerCase()) >=
              0 ||
            Patron.email.toLowerCase().indexOf(self.Search.toLowerCase()) >= 0
          );
        });
      },
    },
  };